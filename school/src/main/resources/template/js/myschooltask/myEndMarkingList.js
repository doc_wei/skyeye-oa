
var rowId = "";

layui.config({
	base: basePath, 
	version: skyeyeVersion
}).extend({
    window: 'js/winui.window'
}).define(['window', 'table', 'jquery', 'winui', 'form', 'laydate'], function (exports) {
	winui.renderColor();
	var $ = layui.$,
		form = layui.form,
		table = layui.table,
		laydate = layui.laydate;
	
	laydate.render({elem: '#year', type: 'year', max: 'date'});

	// 获取当前登陆用户所属的学校列表
	schoolUtil.queryMyBelongSchoolList(function (json) {
		$("#schoolId").html(getDataUseHandlebars(getFileContent('tpl/template/select-option-must.tpl'), json));
		form.render("select");
		// 加载
		initTable();
		initFaculty();
	});
	// form.on('select(schoolId)', function(data) {
	// 	//加载年级
 	// 	initGradeId();
	// });
	//
	// //所属年级
    // function initGradeId(){
	//     showGrid({
    // 	 	id: "gradeId",
    // 	 	url: schoolBasePath + "grademation006",
    // 	 	params: {schoolId: $("#schoolId").val()},
    // 	 	pagination: false,
    // 	 	template: getFileContent('tpl/template/select-option.tpl'),
    // 	 	ajaxSendLoadBefore: function(hdb) {
    // 	 	},
    // 	 	ajaxSendAfter:function (json) {
    // 	 		form.render('select');
    // 	 	}
    //     });
    // }
	// 学校监听事件
	form.on('select(schoolId)', function(data) {
		if(isNull(data.value) || data.value === '请选择'){
			$("#schoolId").html("");
			form.render('select');
		} else {
			// 加载院系
			initFaculty();
		}
	});

	//所属院系
	function initFaculty(){
		showGrid({
			id: "facultyId",
			url: schoolBasePath + "queryFacultyListBySchoolId",
			params: {schoolId: $("#schoolId").val()},
			method: "GET",
			pagination: false,
			template: getFileContent('tpl/template/select-option.tpl'),
			ajaxSendLoadBefore: function(hdb) {
			},
			ajaxSendAfter:function (json) {
				form.render('select');
			}
		});
	}

	// 院系监听事件
	form.on('select(facultyId)', function(data) {
		if(isNull(data.value) || data.value === '请选择'){
			$("#facultyId").html("");
			form.render('select');
		} else {
			// 加载专业
			initMajor();
		}
	});

	// 初始化专业
	function initMajor(){
		showGrid({
			id: "majorId",
			url: schoolBasePath + "queryMajorListByFacultyId",
			method: "GET",
			params: {facultyId: $("#facultyId").val()},
			pagination: false,
			template: getFileContent('tpl/template/select-option.tpl'),
			ajaxSendLoadBefore: function(hdb) {
			},
			ajaxSendAfter:function (json) {
				form.render('select');
			}
		});
	}

	// 专业监听事件
	form.on('select(majorId)', function(data) {
		if(isNull(data.value) || data.value === '请选择'){
			$("#majorId").html("");
			form.render('select');
		} else {
			// 加载科目
			initSubject();
		}
	});

	//初始化科目
	function initSubject(){
		showGrid({
			id: "subjectId",
			url: schoolBasePath + "querySubjectListByMajorId",
			params: {majorId: $("#majorId").val()},
			method: "GET",
			pagination: false,
			template: getFileContent('tpl/template/select-option.tpl'),
			ajaxSendLoadBefore: function(hdb) {},
			ajaxSendAfter:function (json) {
				form.render('select');
			}
		});
	}

	function initTable(){
		table.render({
	        id: 'messageTable',
	        elem: '#messageTable',
	        method: 'post',
	        url: schoolBasePath + 'queryFilterApprovedSurveys',//接口文档在 试卷回答信息表管理schoolMation
	        where: getTableParams(),
	        even: false,
		    page: true,
		    limits: getLimits(),
	    	limit: getLimit(),
	        cols: [[
	        	{ title: systemLanguage["com.skyeye.serialNumber"][languageType], rowspan: '2', type: 'numbers' },
	        	{ field: 'studentName', rowspan: '2', width: 80, title: '姓名',templet:function (d) {
						return d.stuMation?.realName}},
	        	{ field: 'studentNo', rowspan: '2', width: 140, align: 'center', title: '学号',templet:function (d) {
						return d.stuMation?.studentNumber}},
	            { field: 'schoolName', rowspan: '2', width: 150, title: '学校',templet:function (d) {
						return d.schoolMation?.name}},
				{ field: 'facultyName', rowspan: '2', width: 80, align: 'center', title: '院系',templet:function (d) {
						return d.facultyMation?.name}},
				{ field: 'majorName', rowspan: '2', width: 80, align: 'center', title: '专业',templet:function (d) {
						return d.majorMation?.name}},
	            { field: 'surveyName', rowspan: '2', width: 200, title: '试卷名称', templet: function (d) {
			        return '<a lay-event="details" class="notice-title-click">' + d.surveyMation?.surveyName + '</a>';
			    }},
            	{ title: '阅卷信息', align: 'center', colspan: '3'},
            	{ title: '答题信息', align: 'center', colspan: '3'}
	        ],[
		    	{ field: 'markStartTime', title: '开始时间', align: 'center', width: 120},
		        { field: 'markEndTime', title: '结束时间', align: 'center', width: 120},
		        { field: 'markFraction', title: '最后得分', align: 'center', width: 100},
		        { field: 'bgAnDate', title: '开始时间', align: 'center', width: 120},
		        { field: 'endAnDate', title: '结束时间', align: 'center', width: 120},
		        { field: 'totalTime', title: '耗时(分钟)', align: 'center', width: 100}
	        ]
		    ],
		    done: function(json) {
		    	matchingLanguage();
		    }
	    });
	    
	    table.on('tool(messageTable)', function (obj) {
	        var data = obj.data;
	        var layEvent = obj.event;
	        if (layEvent === 'details') { //详情
	        	details(data);
	        }
	    });
		
	    form.render();
	}
	
	
	form.on('submit(formSearch)', function (data) {
        
        if (winui.verifyForm(data.elem)) {
        	refreshTable();
        }
        return false;
    });
    
    //详情
	function details(data) {
		rowId = data.id;
		_openNewWindows({
			url: "../../tpl/examDetail/examPCDetail.html", 
			title: "试卷信息",
			pageId: "examPCDetail",
			area: ['90vw', '90vh'],
			callBack: function (refreshCode) {
			}});
	}
	
	//刷新数据
    $("body").on("click", "#reloadTable", function() {
    	loadTable();
    });
    
    function loadTable() {
    	table.reloadData("messageTable", {where: getTableParams()});
    }
    
    function refreshTable(){
    	table.reloadData("messageTable", {page: {curr: 1}, where: getTableParams()});
    }
    
    function getTableParams() {
    	return {
			holderKey: $("#schoolId").val(),
			holderId:$("#facultyId").val(),
			objectKey: $("#majorId").val(),//专业
			type: $("#studentName").val(),//学生
			// studentNo: $("#studentNo").val(),//学号 刘庆余
			keyword: $("#surveyName").val(),//试卷名
    	};
    }
    
    exports('myEndMarkingList', {});
});
